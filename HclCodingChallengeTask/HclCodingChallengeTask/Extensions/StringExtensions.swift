//
//  StringExtensions.swift
//  HclCodingChallengeTask
//
//  Created by Medikonda AnilKumar on 30/08/21.
//

import Foundation

extension String {
    func isImageTypeUrl() -> Bool {
        
        // image formats which you want to check
        let imageFormats = ["jpg", "png", "gif", "jpeg"]
        
        guard URL(string: self) != nil else {
            return false
        }
        let extensions = (self as NSString).pathExtension
        return imageFormats.contains(extensions)
    }
}
