
//
//  UIButtonExtesnions.swift
//  HclCodingChallengeTask
//  Created by Medikonda AnilKumar on 20/08/21.
//

import UIKit

extension UIButton {
    
    func buttonWithTitle(_ title: String, _ imageName: String) -> UIButton {
        
        let button = UIButton()
        button.setTitle(title, for: .normal)
        button.setTitleColor(UIColor(red: 143/255, green: 150/255, blue: 163/255, alpha: 1), for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.setImage(UIImage(named: imageName), for: .normal)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        button.isEnabled = false //Disabling the Action
        return button
    }
}
