//
//  UIViewExatenions.swift
//  HclCodingChallengeTask
//
//  Created by Medikonda AnilKumar on 20/08/21.
//

import UIKit

extension UIView {
    func addConstraints(withVisualFormat format: String, views: UIView...) {
        
        var viewsDictionary = [String:UIView]()
        for (index, view) in views.enumerated() {
            
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        let constraints = NSLayoutConstraint.constraints(withVisualFormat: format,
                                                         options: NSLayoutConstraint.FormatOptions(),metrics: nil,
                                                         views: viewsDictionary)
        addConstraints(constraints)
    }
}
