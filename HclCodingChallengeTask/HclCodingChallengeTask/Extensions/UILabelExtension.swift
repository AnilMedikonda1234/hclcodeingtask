//
//  UILabelExtension.swift
//  HclCodingChallengeTask
//
//  Created by Medikonda AnilKumar on 29/08/21.
//

import UIKit

extension UILabel {
    
    func labelWithText(_ text: String) -> UILabel {
        
        let label = UILabel()
        label.numberOfLines = 1
        label.font = UIFont.preferredFont(forTextStyle: .caption1)
        label.text = text
        label.textColor = .darkGray
        return label
    }
}
