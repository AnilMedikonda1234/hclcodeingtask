//
//  Utility.swift
//  HclCodingChallengeTask
//
//  Created by Medikonda AnilKumar on 13/08/21.
//

import UIKit

class Utility {
    
    static let shared = Utility()
    private init() {}
    
    // MARK: - Alert Methods
    
    func infoAlertWithoutMessage(_ title: String,viewController: UIViewController) -> Void {
        
        let actionSheetController: UIAlertController = UIAlertController(title:title, message:nil, preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title:"OK" , style: .cancel)
        actionSheetController.addAction(cancelAction)
        viewController.present(actionSheetController, animated: true, completion: nil)
        DispatchQueue.main.async(execute: { viewController.present(actionSheetController, animated: true, completion: nil) })
    }
    
    func infoAlertWithMessage(_ title: String, message:String,viewController:UIViewController) -> Void {
        
        let actionSheetController: UIAlertController = UIAlertController(title:title, message:message, preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title:"OK" , style: .cancel) { action -> Void in }
        actionSheetController.addAction(cancelAction)
        DispatchQueue.main.async(execute: { viewController.present(actionSheetController, animated: true, completion: nil) })
    }
    
    func getCropRatio(width: CGFloat, height: CGFloat) -> CGFloat {
        return CGFloat(width/height)
    }
    
    func  calcualteImageHeight(model: Children ) -> CGFloat {
        
        let data = model.data
        guard let thumbnailHeight =  data?.thumbnailHeight ,let thumbnailWidth = model.data?.thumbnailWidth else { return  UIScreen.main.bounds.width }
        let aspectRatio = Utility.shared.getCropRatio(width: CGFloat(thumbnailWidth), height: CGFloat(thumbnailHeight))
        return UIScreen.main.bounds.width * aspectRatio
    }
    
    func getImage(_ strURL: String?,completionHandler: @escaping (Result<UIImage?, NetworkError>) -> Void) {
        
        let urlString = strURL?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
        guard urlString.isImageTypeUrl(), let imageURL = URL(string: urlString) else {
            completionHandler(.failure(.badURL))
            return
        }
        
        let task = URLSession.shared.dataTask(with: imageURL) { (data, _, error) in
            guard let data = data, error == nil else {
                completionHandler(.failure(.noData))
                return
            }
            let image = UIImage(data: data)
            completionHandler(.success(image))
        }
        task.resume()
    }
    
    // MARK: - SpinnerView Methods
    lazy var transparentView: UIView = {
        
        let transparentView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 100))
        transparentView.backgroundColor = Constants.Colours.viewColor.withAlphaComponent(Constants.setAlpha)
        transparentView.isUserInteractionEnabled = false
        return transparentView
    }()
    
    lazy var gifImage: UIImageView = {
        
        var gifImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 200, height: 60))
        gifImage.contentMode = .scaleAspectFit
        gifImage.center = transparentView.center
        gifImage.isUserInteractionEnabled = false
        gifImage.loadGif(name: Constants.gifName)
        return gifImage
    }()
    
    func createSpinnerForFootetView() -> UIView {
        
        let footerView = transparentView
        transparentView.addSubview(gifImage)
        transparentView.bringSubviewToFront(gifImage)
        return footerView
    }
}

