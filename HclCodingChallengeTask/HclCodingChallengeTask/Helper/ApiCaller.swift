//
//  ApiCaller.swift
//  HclCodingChallengeTask
//
//  Created by Medikonda AnilKumar on 13/08/21.
//

import Foundation

enum NetworkError: Error {
    case badURL
    case invalidData
    case badrequest
    case noData
}


class ApiCaller {
    
    static let shared = ApiCaller()
    private init() {}
    
    func requestGETURL(_ strURL: String, completionHandler: @escaping (Result<Data, NetworkError>) -> Void) {
        
        let urlString = strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        guard let url = URL(string: urlString) else {
            completionHandler(.failure(.badURL))
            return
        }
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            guard let data = data , error == nil  else {
                completionHandler(.failure(.noData))
                return
            }
            completionHandler(.success(data))
        }
        dataTask.resume()
    }
}
