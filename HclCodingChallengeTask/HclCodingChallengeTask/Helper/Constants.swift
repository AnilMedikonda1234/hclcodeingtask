//
//  Constants.swift
//  HclCodingChallengeTask
//
//  Created by Medikonda AnilKumar on 13/08/21.
//

import UIKit

struct Constants {
    
    struct URLs {
        static let  postsApi = "http://www.reddit.com/.json"
        static let paginationApi = "\(postsApi)?after+"
    }
    
    struct Images {
        static  let privacyImage = UIImage(named: "icon-globe")
        static  let profilePlaceHolderImage = UIImage(named: "icon_profile")
        static  let postPlaceHolderImage = UIImage(named: "Placeholder")
        static  let likeImage = UIImage(named: "icon-up")
        static  let iconMessage = UIImage(named: "icon-comment")
        static  let share = UIImage(named: "icons-share")
        static  let globe =   UIImage(named: "icon-globe")
        static  let postImage =   UIImage(named: "3")
        
    }
    
    struct  Colours {
        static let viewColor: UIColor = .black
    }
    
    static let  PostHeight: CGFloat = 8 + 44 + 4 + 8 + 0.4 + 44 //Calculating Height
    static let  ErrorMessage = "SomeThing went wrong, please try again later"
    
    //Mixed ConstValves
    static let gifName: String = "demoImage.giftImage"
    static let setAlpha: CGFloat = 0.5
}

struct Identifier {
    static let PostsTableCell = "PostsListCell"
}



