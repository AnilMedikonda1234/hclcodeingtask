//
//  PostsTableCell.swift
//  HclCodingChallengeTask
//  Created by Medikonda AnilKumar on 16/08/21.
//

import UIKit

class PostsListTableViewCell: UITableViewCell {
    
    private lazy var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        imageView.image = Constants.Images.profilePlaceHolderImage
        return imageView
    }()
    
    private lazy var usernameLabel: UILabel = {
        let label = UILabel().labelWithText("Alexey Parkhomenko")
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        return label
    }()
    
    private lazy var postStatLabel: UILabel = {
        let label = UILabel().labelWithText("12 Likes     8 Comments     2 Shares")
        postImageView.image = Constants.Images.postImage
        return label
    }()
    
    private lazy var captionTextView: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 14)
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.isSelectable = false
        return textView
    }()
    
    lazy var postImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.masksToBounds = true
        imageView.image = Constants.Images.postPlaceHolderImage
        return imageView
    }()
    
    private lazy var timeAgoLabel: UILabel = {
        let label =  UILabel().labelWithText("Now")
        return label
        
    }()
    
    private lazy var privacyImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.layer.masksToBounds = true
        imageView.image = Constants.Images.iconMessage
        return imageView
    }()
    
    // size of this view we will make using auto layout constraints
    private lazy var postDividerView: UIView! = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 226/255.0, green: 228/255.0, blue: 232/255.0, alpha: 1)
        return view
    }()
    
    private lazy var likeButton: UIButton = UIButton().buttonWithTitle("Like", "icon-up")
    private lazy var commentButton: UIButton = UIButton().buttonWithTitle("Comment", "icons8-comments")
    private lazy var shareButton: UIButton = UIButton().buttonWithTitle("Share", "icons-share")
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func  setupUI() {
        
        backgroundColor = UIColor.white
        addSubViews()
        addConstraints()
        commentButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
    }
    
    private func addConstraints() {
        
        // profileImageView and usernameLabel constraints
        addConstraints(withVisualFormat: "H:|-8-[v0(44)]-8-[v1]-8-|", views: profileImageView, usernameLabel)
        addConstraints(withVisualFormat: "V:|-8-[v0]", views: usernameLabel)
        
        //constraints for timeAgoLabel + privacyImageView
        addConstraints(withVisualFormat: "H:[v0]-8-[v1]-4-[v2(10)]", views: profileImageView, timeAgoLabel, privacyImageView)
        addConstraints(withVisualFormat: "V:[v0]-4-[v1]", views: usernameLabel, timeAgoLabel)
        addConstraints(withVisualFormat: "V:[v0]-6-[v1(10)]", views: usernameLabel, privacyImageView)
        
        // constraints for the captionTextView
        addConstraints(withVisualFormat: "H:|[v0]|", views: captionTextView)
        
        // constraints for the postImageView
        addConstraints(withVisualFormat: "H:|[v0]|", views: postImageView)
        
        // constraints for the postDividerView
        addConstraints(withVisualFormat: "H:|-8-[v0]-8-|", views: postDividerView)
        
        // constraints for the buttons
        addConstraints(withVisualFormat: "H:|[v0(v2)][v1(v2)][v2]|", views: likeButton, commentButton, shareButton)
        addConstraints(withVisualFormat: "V:[v0(44)]|", views: commentButton)
        addConstraints(withVisualFormat: "V:[v0(44)]|", views: shareButton)
        
        // constraints vertical
        addConstraints(withVisualFormat: "V:|-8-[v0(44)]-4-[v1][v2]-8-[v4(0.4)][v5(44)]|", views: profileImageView, captionTextView, postImageView, postStatLabel, postDividerView, likeButton)
        
    }
    
    private func addSubViews() {
        
        backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        addSubview(usernameLabel)
        addSubview(profileImageView)
        addSubview(timeAgoLabel)
        addSubview(privacyImageView)
        addSubview(captionTextView)
        addSubview(postImageView)
        addSubview(postDividerView)
        addSubview(likeButton)
        addSubview(commentButton)
        addSubview(shareButton)
    }
    
    
    func setupData(model: Children, indexPath: IndexPath) {
        
        let data = model.data
        usernameLabel.text = data?.authorFullname ?? " "
        usernameLabel.textColor = data?.linkFlairTextColor == "dark" ? UIColor.darkGray:UIColor.lightGray
        captionTextView.text = data?.title
        setupStatusLabel(data)
        
        Utility.shared.getImage(data?.thumbnail) {  [weak self] (result) in
            
            guard let self = self else { return }
            switch result {
            case .success(let image):
                DispatchQueue.main.async {
                    self.postImageView.image =  image
                }
            case .failure(_):
                DispatchQueue.main.async {
                    self.postImageView.image =  Constants.Images.postPlaceHolderImage
                }
            }
        }
    }
    
    private func setupStatusLabel(_ data: PostChildData?) {
        
        let likeStat =  "\(data?.score?.roundedWithAbbreviations ?? " ") Likes"
        likeButton.setTitle(likeStat, for: .normal)
        
        let commentStat = "\(data?.numComments?.roundedWithAbbreviations ?? " ") Comments"
        commentButton.setTitle(commentStat, for: .normal)
        
        let shareStat = "\(data?.totalAwardsReceived?.roundedWithAbbreviations ?? " ") Shares"
        shareButton.setTitle(shareStat, for: .normal)
    }
}
