//
//  PostsTableViewController.swift
//  HclCodingChallengeTask
//  Created by Medikonda AnilKumar on 16/08/21.
//

import UIKit

class PostsTableViewController: UITableViewController {
    
    var viewModel: PostsListTableViewModel? = nil
    let transparentView = Utility.shared.transparentView
    let gifImage = Utility.shared.gifImage
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    func setup () {
        
        title = "Posts"
        tableView.backgroundColor = #colorLiteral(red: 0.8640883565, green: 0.9178858399, blue: 0.9996795058, alpha: 1)
        tableView.alwaysBounceVertical = true
        tableView.register(PostsListTableViewCell.self, forCellReuseIdentifier: Identifier.PostsTableCell)
        tableView.separatorStyle = .none
        
        viewModel = PostsListTableViewModel()
        setUpApiCall(false)
        
    }
    
    func setUpApiCall( _ isPagenation: Bool) {
        
        if isPagenation {
            tableView.tableFooterView = Utility.shared.createSpinnerForFootetView()
        } else {
            CustomLoader.instance.showLoaderView()
        }
        
        viewModel?.getPostsApi(isPagenation, completion: {  [weak self] (error) in
            
            CustomLoader.instance.hideLoaderView()
            guard let self = self else { return }
            
            guard error == nil else {
                Utility.shared.infoAlertWithMessage("error", message: " \(error?.localizedDescription ?? Constants.ErrorMessage)", viewController: self)
                return
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
                guard isPagenation else { return }
                self.tableView.tableFooterView = nil
            }
        })
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.posts.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: Identifier.PostsTableCell, for: indexPath) as? PostsListTableViewCell else {
            fatalError()
        }
        guard let post = viewModel?.posts[indexPath.row] else {
            fatalError()
        }
        cell.selectionStyle = .none
        cell.setupData(model: post, indexPath: indexPath)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel?.setUpImageHeight(indexPath) ??  300
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let lastElement = self.viewModel?.posts.count, indexPath.row == (lastElement - 1) {
            setUpApiCall(true)
        }
    }
}
