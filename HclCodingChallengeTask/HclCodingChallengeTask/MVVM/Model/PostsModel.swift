//
//  PostsModel.swift
//  HclCodingChallengeTask
//  Created by Medikonda AnilKumar on 14/08/21.
//

struct PostsModel : Decodable {
    
    var data: PostsData?
    
    enum CodingKeys: String, CodingKey {
        case data
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent(PostsData.self, forKey: .data)
    }
}

