//
//  Children.swift
//  HclCodingChallengeTask
//
//  Created by Medikonda AnilKumar on 20/08/21.
//

struct Children : Decodable {
    
    let data: PostChildData?
    
    enum CodingKeys: String, CodingKey {
        case data
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent(PostChildData.self, forKey: .data)
    }
}

