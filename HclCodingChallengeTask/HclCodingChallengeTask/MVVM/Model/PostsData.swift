//
//  PostsData.swift
//  HclCodingChallengeTask
//
//  Created by Medikonda AnilKumar on 20/08/21.
//

struct PostsData : Decodable {
    
    var after: String?
    let children: [Children]?
    
    enum CodingKeys: String, CodingKey {
        case children,after
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        children = try values.decodeIfPresent([Children].self, forKey: .children)
        after = try values.decodeIfPresent(String.self, forKey: .after)
    }
}

