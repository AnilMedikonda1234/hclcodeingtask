//
//  PostChildData.swift
//  HclCodingChallengeTask
//
//  Created by Medikonda AnilKumar on 20/08/21.
//


struct PostChildData : Decodable {
    
    var authorFullname: String?
    var title: String?
    var thumbnailHeight: Int?
    var thumbnail: String?
    var numComments: Int?
    var totalAwardsReceived: Int?
    var linkFlairTextColor: String?
    var score: Int?
    var thumbnailWidth: Int?
    var ups: Int?
    
    enum CodingKeys: String, CodingKey {
        case title,ups,thumbnail,score
        case authorFullname = "author_fullname"
        case thumbnailHeight = "thumbnail_height"
        case numComments = "num_comments"
        case totalAwardsReceived = "total_awards_received"
        case linkFlairTextColor = "link_flair_text_color"
        case thumbnailWidth = "thumbnail_width"
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        authorFullname =  try values.decodeIfPresent(String.self, forKey: .authorFullname)
        title =  try values.decodeIfPresent(String.self, forKey: .title)
        numComments = try values.decodeIfPresent(Int.self, forKey: .numComments)
        totalAwardsReceived =  try values.decodeIfPresent(Int.self, forKey: .totalAwardsReceived)
        linkFlairTextColor = try values.decodeIfPresent(String.self, forKey: .linkFlairTextColor)
        score = try values.decodeIfPresent(Int.self, forKey: .score)
        thumbnail =  try values.decodeIfPresent(String.self, forKey: .thumbnail)
        thumbnailWidth = try values.decodeIfPresent(Int.self, forKey: .thumbnailWidth)
        thumbnailHeight =   try values.decodeIfPresent(Int.self, forKey: .thumbnailHeight)
    }
}



