//
//  PostsViewModal.swift
//  HclCodingChallengeTask
//  Created by Medikonda AnilKumar on 16/08/21.
//

import UIKit

class PostsListTableViewModel {
    
    var postResponseModel: PostsModel? = nil
    var posts =  [Children]()
    var after = ""
    
    func getPostsApi (_ isPagination: Bool, completion: @escaping (Error?) -> Void) {
        
        let postApi = isPagination ? "\(Constants.URLs.paginationApi)\(after)" : Constants.URLs.postsApi
        
        ApiCaller.shared.requestGETURL(postApi) { [weak self] (result) in
            guard let self = self else { return }
            switch result {
            
            case .success(let data):
                do {
                    self.postResponseModel = try JSONDecoder().decode(PostsModel.self, from: data)
                } catch {
                    completion(error)
                }
                
                completion(nil)
                
                guard let postResponseModel = self.postResponseModel , let data = postResponseModel.data?.children else { return }
                self.after = postResponseModel.data?.after ?? ""
                self.posts.append(contentsOf: data)
                
            case .failure(let error):
                completion(error)
            }
        }
    }
    
    // Calculate Image Height
    func setUpImageHeight(_ indexPath: IndexPath) -> CGFloat {
        
        let model = posts[indexPath.row]
        let imageHeight =  Utility.shared.calcualteImageHeight(model: model)
        return imageHeight  + Constants.PostHeight
    }
}



