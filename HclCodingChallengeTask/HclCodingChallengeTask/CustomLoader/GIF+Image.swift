//
//  GIF+Imaage.swift
//  HclCodingChallengeTask
//  Created by Medikonda AnilKumar on 14/08/21.
//

import UIKit

extension UIImageView {
    
    func loadGif(name: String) {
        
        DispatchQueue.global().async {
            let image = UIImage.gif(name: name)
            DispatchQueue.main.async {
                self.image = image
            }
        }
    }
}

