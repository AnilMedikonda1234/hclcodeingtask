//
//  CustomLoader.swift
//  HclCodingChallengeTask
//
//  Created by Medikonda AnilKumar on 14/08/21.
//


import UIKit

class CustomLoader: UIView {
    
    static let instance = CustomLoader()
    var viewColor: UIColor = .black
    var setAlpha: CGFloat = 0.5
    var gifName: String = "demoImage.giftImage"
    
    lazy var transparentView: UIView = {
        let transparentView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        transparentView.backgroundColor = viewColor.withAlphaComponent(setAlpha)
        transparentView.isUserInteractionEnabled = false
        return transparentView
    }()
    
    lazy var gifImage: UIImageView = {
        var gifImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 200, height: 60))
        gifImage.contentMode = .scaleAspectFit
        gifImage.center = transparentView.center
        gifImage.isUserInteractionEnabled = false
        gifImage.loadGif(name: gifName)
        return gifImage
    }()
    
    func showLoaderView() {
        
        DispatchQueue.main.async {
            self.addSubview(self.transparentView)
            self.transparentView.addSubview(self.gifImage)
            self.transparentView.bringSubviewToFront(self.gifImage)
            UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.addSubview(self.transparentView)
        }
    }
    
    func hideLoaderView() {
        
        DispatchQueue.main.async {
            self.transparentView.removeFromSuperview()
        }
    }
}
