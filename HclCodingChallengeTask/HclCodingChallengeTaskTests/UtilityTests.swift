//
//  UtilityTests.swift
//  HclCodingChallengeTaskTests
//
//  Created by Medikonda AnilKumar on 24/08/21.
//

import XCTest
@testable import HclCodingChallengeTask

class UtilityTests: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testAlertClass() {
        
        let vc = UIViewController()
        Utility.shared.infoAlertWithMessage("Error", message: "This Message Nill", viewController: vc)
        Utility.shared.infoAlertWithoutMessage("Error", viewController: vc)
    }
    
    func testGetCreateSpinnerForFootetView() {
        let spinnerView = Utility.shared.createSpinnerForFootetView()
        XCTAssertNotNil(spinnerView, "spinnerView  is nil ")
    }
    
    func testCropRation() {
        
        let cropRatio = Utility.shared.getCropRatio(width: 250.0, height: 414)
        XCTAssertNotNil(cropRatio, "CropRatio is nil  Valve")
    }
    
    func testCustomLoaderView() {
        
        CustomLoader.instance.hideLoaderView()
    }
    
}
