//
//  ExtesnionTests.swift
//  HcliOSCodingChallengeTaskTests
//  Created by Medikonda AnilKumar on 15/08/21.
//

import XCTest
@testable import HclCodingChallengeTask

class ExtesnionTests: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testImageCorpRatioTests () {
        
        let image = Constants.Images.postPlaceHolderImage
        guard let imageObject = image  else { return }
        let ratio = imageObject.getCropRatio()
        XCTAssertNotNil(ratio, "Ratio is image nil ")
    }
    
    func testGiftImageNotNil () {
        
        let image = UIImage.gif(url: "https://cdn.maikoapp.com/3d4b/4quqa/150.png")
        XCTAssertNotNil(image, "Image is nil ")
    }
    
    func testGiftImageNil () {
        
        let image = UIImage.gif(url: "https://cdn.maikoapp.com/3d4b/4quqa")
        XCTAssertNil(image, "Image is nil ")
    }
    
    func testgetImageFromDataTest () {
        
        do {
            let data = try Data.init(contentsOf: URL(string: "https://cdn.maikoapp.com/3d4b/4quqa/150.png")!)
            let image = UIImage.gif(data: data)
            XCTAssertNotNil(image, "Image is nil")
        } catch {
            XCTFail("Image data is nil")
        }
    }
    
    func testgetImageFromAsset () {
        
        let image = UIImage.gif(asset: "icon-like")
        XCTAssertNil(image, "Image is nil")
    }
    
    func testButtonExtensionTests () {
        
        let button = UIButton().buttonWithTitle("Hello , World", "icon-like")
        XCTAssertNotNil(button, "Button is nil")
    }
    
    func testUILabelExtensionTests () {
        
        let label = UILabel().labelWithText("Hello, World")
        XCTAssertNotNil(label, "Button is nil")
    }
    
    
}
