//
//  ApiCallerTests.swift
//  HclCodingChallengeTaskTests
//
//  Created by Medikonda AnilKumar on 24/08/21.
//

import XCTest
@testable import HclCodingChallengeTask

class ApiCallerTests: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testInValidURLTests () {
        
        ApiCaller.shared.requestGETURL("InvalidURlTests") {   (result) in
            switch result {
            case .success(let data):
                XCTAssertNil(data)
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
        
        func testValidApiCallTests () {
            
            ApiCaller.shared.requestGETURL(Constants.URLs.postsApi) {   (result) in
                switch result {
                case .success(let data):
                    XCTAssertNotNil(data)
                    do {
                        let responseData = try JSONDecoder().decode(PostsModel.self, from: data)
                        XCTAssertNotNil(responseData)
                    } catch {
                        XCTFail()
                    }
                case .failure( _):
                    XCTFail()
                }
            }
        }
    }
}
